// +build linux

package io_uring

import (
	"sync/atomic"

	iouring_syscall "gitee.com/childewang/iouring-go/syscall"
)

// CompletionQueue ...
type CompletionQueue struct {
	ptr uintptr

	head        *uint32
	tail        *uint32
	ringMask    *uint32
	ringEntries *uint32
	flags       *uint32
	overflow    *uint32
	cqes        []iouring_syscall.IOUringCQE
	size        uint32
}

func (cq *CompletionQueue) peek() (cqe *iouring_syscall.IOUringCQE) {
	head := *cq.head
	if head != atomic.LoadUint32(cq.tail) {
		cqe = &cq.cqes[head&*cq.ringMask]
	}
	return
}

func (cq *CompletionQueue) advance(num uint32) {
	if num != 0 {
		atomic.AddUint32(cq.head, num)
	}
}
