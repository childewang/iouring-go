// +build linux

package io_uring

import (
	"fmt"
	"os"
	"reflect"
	"syscall"
	"unsafe"

	iouring_syscall "gitee.com/childewang/iouring-go/syscall"
)

var (
	uint32Size = uint32(unsafe.Sizeof(uint32(0)))
	sqeSize    = uint32(unsafe.Sizeof(iouring_syscall.IOUringSQE{}))
)

func mmapIOURing(iouring *IOUring) (err error) {
	defer func() {
		if err != nil {
			munmapIOURing(iouring)
		}
	}()
	iouring.sq = new(SubmissionQueue)
	iouring.cq = new(CompletionQueue)

	if err = mmapSQ(iouring); err != nil {
		return err
	}

	// if (iouring.params.Features & iouring_syscall.IORING_FEAT_SINGLE_MMAP) != 0 {
	// 	iouring.cq.ptr = iouring.sq.ptr
	// }

	if err = mmapCQ(iouring); err != nil {
		return err
	}

	if err = mmapSQEs(iouring); err != nil {
		return err
	}
	return nil
}

func mmapSQ(iouring *IOUring) (err error) {
	sq := iouring.sq
	params := iouring.params

	sq.size = params.SQOffset.Array + params.SQEntries*uint32Size
	ptr, err := mmap(iouring.ringFd, sq.size, iouring_syscall.IORING_OFF_SQ_RING)
	if err != nil {
		sq = nil
		return fmt.Errorf("mmap sq ring: %w", err)
	}

	sq.head = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.Head)))
	sq.tail = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.Tail)))
	sq.ringMask = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.RingMask)))
	sq.ringEntries = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.RingEntries)))
	sq.flags = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.Flags)))
	sq.dropped = (*uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.Dropped)))
	// sq.array = *(*[]uint32)(unsafe.Pointer(ptr + uintptr(params.SQOffset.Array)))
	sq.array = *(*[]uint32)(unsafe.Pointer(&reflect.SliceHeader{
		Data: ptr + uintptr(params.SQOffset.Array),
		Len:  int(params.SQEntries),
		Cap:  int(params.SQEntries),
	}))

	return nil
}

func mmapCQ(iouring *IOUring) (err error) {
	cq := iouring.cq
	params := iouring.params

	cq.size = params.CQOffset.Cqes + params.CQEntries*uint32Size

	ptr, err := mmap(iouring.ringFd, cq.size, iouring_syscall.IORING_OFF_CQ_RING)
	if err != nil {
		cq = nil
		return fmt.Errorf("mmap cq ring: %w", err)
	}

	cq.head = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.Head)))
	cq.tail = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.Tail)))
	cq.ringMask = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.RingMask)))
	cq.ringEntries = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.RingEntries)))
	cq.flags = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.Flags)))
	cq.overflow = (*uint32)(unsafe.Pointer(ptr + uintptr(params.CQOffset.Overflow)))

	cq.cqes = *(*[]iouring_syscall.IOUringCQE)(
		unsafe.Pointer(&reflect.SliceHeader{
			Data: ptr + uintptr(params.CQOffset.Cqes),
			Len:  int(params.CQEntries),
			Cap:  int(params.CQEntries),
		}))

	return nil
}

func mmapSQEs(iouring *IOUring) error {
	params := iouring.params

	fmt.Println(iouring.ringFd)
	fmt.Printf("%+v\n", iouring)
	fmt.Printf("%+v\n", iouring.params)
	fmt.Println(sqeSize)
	fmt.Println(params.SQEntries * sqeSize)
	fmt.Println(iouring_syscall.IORING_OFF_SQES)
	ptr, err := mmap(iouring.ringFd, params.SQEntries*sqeSize, iouring_syscall.IORING_OFF_SQES)
	if err != nil {
		return fmt.Errorf("mmap sqe array: %w", err)
	}

	iouring.sq.sqes = *(*[]iouring_syscall.IOUringSQE)(unsafe.Pointer(&reflect.SliceHeader{
		Data: ptr,
		Len:  int(params.SQEntries),
		Cap:  int(params.SQEntries),
	}))

	fmt.Println("sqe length: ", len(iouring.sq.sqes))

	return nil
}

func munmapIOURing(iouring *IOUring) error {
	if iouring.sq != nil {
		if len(iouring.sq.sqes) != 0 {
			err := munmap(uintptr(unsafe.Pointer(&iouring.sq.sqes[0])), uint32(len(iouring.sq.sqes))*sqeSize)
			if err != nil {
				return fmt.Errorf("ummap sqe array: %w", err)
			}
			iouring.sq.sqes = nil
		}

		if err := munmap(uintptr(unsafe.Pointer(iouring.sq.head)), iouring.sq.size); err != nil {
			return fmt.Errorf("munmap sq: %w", err)
		}
		// if iour.sq.ptr == iouring.cq.ptr {
		// 	iouring.cq = nil
		// }
		iouring.sq = nil
	}

	if iouring.cq != nil {
		if err := munmap(uintptr(unsafe.Pointer(iouring.cq.head)), iouring.cq.size); err != nil {
			return fmt.Errorf("munmap cq: %w", err)
		}
		iouring.cq = nil
	}

	return nil
}

func mmap(fd int, length uint32, offset uint64) (uintptr, error) {
	ptr, _, errno := syscall.Syscall6(
		syscall.SYS_MMAP,
		0,
		uintptr(length),
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED|syscall.MAP_POPULATE,
		uintptr(fd),
		uintptr(offset),
	)
	if errno != 0 {
		return 0, os.NewSyscallError("mmap", errno)
	}
	return uintptr(ptr), nil
}

func munmap(ptr uintptr, length uint32) error {
	_, _, errno := syscall.Syscall(
		syscall.SYS_MUNMAP,
		ptr,
		uintptr(length),
		0,
	)
	if errno != 0 {
		return os.NewSyscallError("munmap", errno)
	}
	return nil
}
