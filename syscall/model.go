package iouring_syscall

// sqe->flags
const (
	// use fixed fileset
	IOSQE_FLAGS_FIXED_FILE uint8 = 1 << iota
	// issue after inflight IO
	IOSQE_FLAGS_IO_DRAIN
	// links next sqe
	IOSQE_FLAGS_IO_LINK
	// like LINK, but stronger
	IOSQE_FLAGS_IO_HARDLINK
	// always go async
	IOSQE_FLAGS_ASYNC
	// select buffer from sqe->buf_group
	IOSQE_FLAGS_BUFFER_SELECT
)

// sqe->opcode
const (
	IORING_OP_NOP uint8 = iota
	IORING_OP_READV
	IORING_OP_WRITEV
	IORING_OP_FSYNC
	IORING_OP_READ_FIXED
	IORING_OP_WRITE_FIXED
	IORING_OP_POLL_ADD
	IORING_OP_POLL_REMOVE
	IORING_OP_SYNC_FILE_RANGE
	IORING_OP_SENDMSG
	IORING_OP_RECVMSG
	IORING_OP_TIMEOUT
	IORING_OP_TIMEOUT_REMOVE
	IORING_OP_ACCEPT
	IORING_OP_ASYNC_CANCEL
	IORING_OP_LINK_TIMEOUT
	IORING_OP_CONNECT
	IORING_OP_FALLOCATE
	IORING_OP_OPENAT
	IORING_OP_CLOSE
	IORING_OP_FILES_UPDATE
	IORING_OP_STATX
	IORING_OP_READ
	IORING_OP_WRITE
	IORING_OP_FADVISE
	IORING_OP_MADVISE
	IORING_OP_SEND
	IORING_OP_RECV
	IORING_OP_OPENAT2
	IORING_OP_EPOLL_CTL
	IORING_OP_SPLICE
	IORING_OP_PROVIDE_BUFFERS
	IORING_OP_REMOVE_BUFFERS
	IORING_OP_TEE
	IORING_OP_SHUTDOWN
	IORING_OP_RENAMEAT
	IORING_OP_UNLINKAT
	IORING_OP_LAST
)

const (
	IORING_SQ_NEED_WAKEUP uint32 = 1 << iota
	IORING_SQ_CQ_OVERFLOW
)

// sqe->fsync_flags
const (
	IORING_FSYNC_DATASYNC uint32 = 1 << iota
)

// sqe->timeout_flags
const (
	IORING_TIMEOUT_ABS uint32 = 1 << iota
	IORING_TIMEOUT_UPDATE
)

/*
 * sqe->splice_flags
 * extends splice(2) flags
 */
const (
	SPLICE_F_FD_IN_FIXED uint32 = 1 << 31
)

/*
 * POLL_ADD flags. Note that since sqe->poll_events is the flag space, the
 * command flags for POLL_ADD are stored in sqe->len.
 *
 * IORING_POLL_ADD_MULTI	Multishot poll. Sets IORING_CQE_F_MORE if
 *				the poll handler will continue to report
 *				CQEs on behalf of the same SQE.
 *
 * IORING_POLL_UPDATE		Update existing poll request, matching
 *				sqe->addr as the old user_data field.
 */
const (
	IORING_POLL_ADD_MULTI uint32 = 1 << iota
	IORING_POLL_UPDATE_EVENTS
	IORING_POLL_UPDATE_USER_DATA
)

// IOUringCQE ...
type IOUringCQE struct {
	UserData uint64
	Result   int32
	Flags    uint32
}

/*
 * cqe->flags
 *
 * IORING_CQE_F_BUFFER	If set, the upper 16 bits are the buffer ID
 * IORING_CQE_F_MORE	If set, parent SQE will generate more CQE entries
 */
const (
	IORING_CQE_F_BUFFER uint32 = 1 << iota
	IORING_CQE_F_MORE
)

const IORING_CQE_BUFFER_SHIFT = 16

/*
 * Magic offsets for the application to mmap the data it needs
 */
const (
	IORING_OFF_SQ_RING uint64 = 0
	IORING_OFF_CQ_RING uint64 = 0x8000000
	IORING_OFF_SQES    uint64 = 0x10000000
)
