package main

import (
	"fmt"
	"io"
	"os"
)

func copyFile(src, dest *os.File) {
	io.Copy(dest, src)
}

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Usage: %s file1 file2\n", os.Args[0])
		return
	}

	src, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Printf("Open src file failed: %v\n", err)
		return
	}
	defer src.Close()

	dest, err := os.Create(os.Args[2])
	if err != nil {
		fmt.Printf("create dest file failed: %v\n", err)
		return
	}
	defer dest.Close()

	copyFile(src, dest)
}
