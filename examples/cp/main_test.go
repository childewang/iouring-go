package main

import (
	"fmt"
	"os"
	"testing"
)

func BenchmarkCopy(b *testing.B) {
	src, err := os.Open("../../1.txt")
	if err != nil {
		fmt.Printf("Open src file failed: %v\n", err)
		return
	}
	defer src.Close()

	dest, err := os.Create("../../2.txt")
	if err != nil {
		fmt.Printf("create dest file failed: %v\n", err)
		return
	}
	defer dest.Close()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		copyFile(src, dest)
	}
}
